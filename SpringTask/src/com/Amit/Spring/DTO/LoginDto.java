package com.Amit.Spring.DTO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="Register_form")
public class LoginDto implements Serializable
{
	
	public LoginDto() 
	{
		System.out.println(this.getClass().getSimpleName()+" "+"CREATED....");
	}
	@Id
	@Column(name="s_uname")
	private String uname;
	@Column(name="s_pass")
	private String pass;
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
}
