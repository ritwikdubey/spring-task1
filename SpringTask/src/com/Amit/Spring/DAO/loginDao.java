package com.Amit.Spring.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Amit.Spring.DTO.LoginDto;
import com.Amit.Spring.DTO.RegisterDto;
@Component
public class loginDao
{
	@Autowired
	private SessionFactory factory;
	public loginDao()
	{
		System.out.println(this.getClass().getSimpleName()+" "+"created...");
	}
	
	public boolean getdata(LoginDto regdto)
	{LoginDto
		  System.out.println("saving inside dao...");
		  System.out.println("we have to use hibernate");
		  System.out.println("Session Factory...."+factory);
		  
		  Session  session = factory.openSession();
		  
		  String hql="SELECT B FROM  LoginDto  B WHERE B.uname=regdto.getUname() and B.pass=regdto.getPass()";
		
	
		
		try
		{
			Query query=session.createQuery(hql);
			LoginDto rdto=(LoginDto) query.uniqueResult();
			if(rdto!=null)
			{
				System.out.println("the fetched data are available");
				if( rdto.getUname().equals(regdto.getUname()) && rdto.getPass().equals(regdto.getPass()))
			  {
					System.out.println("validation checking of data");
				System.out.println("the login data is valid");
				return true;
			  }
		    }
			else
			{
				System.out.println("the login data is invalid");
			}
		}
		finally
		{
			session.close();
		}
		return false;
	}

}
