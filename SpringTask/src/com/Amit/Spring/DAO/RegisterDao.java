package com.Amit.Spring.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Amit.Spring.DTO.RegisterDto;
@Component
public class RegisterDao 
{
	@Autowired
	private SessionFactory factory;
		public RegisterDao() 
		{
			System.out.println(this.getClass().getSimpleName()+" "+"created.....");
		}
		public boolean save(RegisterDto regdto)
		{
			System.out.println("saving inside dao...");
			System.out.println("we have to use hibernate");
			System.out.println("Session Factory...."+factory);
			Session  session = factory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(regdto);
			tx.commit();
			session.close();
			return true;
		}
}
