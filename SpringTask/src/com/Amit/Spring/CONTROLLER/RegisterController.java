package com.Amit.Spring.CONTROLLER;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import com.Amit.Spring.DTO.RegisterDto;
import com.Amit.Spring.SERVICE.RegisterService;
@Component
@RequestMapping("/")
public class RegisterController 
{
	@Autowired
	private RegisterService registerservice;
public RegisterController()
{
System.out.println(this.getClass().getSimpleName()+" "+"created.....");
}

@RequestMapping(value= "/Signup.do")
public String signUp( RegisterDto regdto,HttpServletRequest request)
{
	 System.out.println("we are inside the signUpController signUp method ");
	 System.out.println(regdto+"  "+"details.... ");
	 request.setAttribute("regdto", regdto);
	 boolean sucess=registerservice.regservice(regdto);
		if(sucess)
		return "/sucess.jsp";
		return "/SignUp.jsp";
	
	 
}
}
